<?php add_theme_support('post-thumbnails'); ?>
<?php register_sidebar( array(
    'name' => __( 'footer' ),
    'id' => 'footer-widget-area',
    'description' => __( 'footer area' ),
    'before_widget' => '<li id="%1$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
) );
?>
    <div class="clear"></div>
<div class="footer">
    <p class="credits">© 2005—<?php echo date('Y'); ?>. Юридическая фирма «ЮФК»</p>
    <p class="tadatuta-link"><a href="http://tadatuta.ru/">Создание сайта</a> — студия «Тадатута»</p>
    <form method="get" name="searchform" id="searchform" action="<?php bloginfo('siteurl')?>">
        <input type="text" name="s" id="s" placeholder="Поиск" class="search-input" required="true"/>
        <input id="btnSearch" type="submit" name="submit" value="<?php _e('Найти'); ?>" class="search-button"/>
    </form>
    <?php if ( is_active_sidebar( 'footer-widget-area' ) ) : ?>
        <div id="footer" role="complementary">
            <ul>
                <?php dynamic_sidebar( 'footer-widget-area' ); ?>
            </ul>
        </div>
    <?php endif; ?>
</div>
</div>


    <?php // Подключение библиотеки карты ?>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&amp;lang=ru-RU" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
    <?php wp_footer(); ?>
    <script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
    </body>
</html>

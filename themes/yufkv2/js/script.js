$(function() {

    var
        navTitle = $('.menu-title'),
        nav = $('.menu-nav');

    // navTitle.on('click', function() {
    //     nav.toggleClass('menu-nav_open menu-nav_closed');
    // });
    $(document).on('click', function(e){
        if($(e.target).hasClass('menu-title')){
            nav.toggleClass('menu-nav_open menu-nav_closed');
        } else {
            nav.removeClass('menu-nav_open');
            nav.addClass('menu-nav_closed');
        }
    });

    $('#searchform').on('submit', function(e) {
        if(!$('.search-input').val()){
            return false;
        }
    });

})

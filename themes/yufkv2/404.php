<?php get_header(); ?>
    <div class="content 404">
        <h1 class="entry-title">Извините запрашиваемая вами страница не найдена</h1>
        <p>Воспользуйтесь поиском или вернитесь на <a href="/">главную</a></p>
    </div>

<?php get_footer(); ?>